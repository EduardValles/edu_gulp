/*
* Dependencias
*/
var gulp = require('gulp'),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify');
  autoprefixer = require('gulp-autoprefixer');
  imagemin = require('gulp-tinypng');

 
gulp.task('demo', function () {
  gulp.src('js/*.js')
  .pipe(concat('todo.js'))
  .pipe(uglify())
  .pipe(gulp.dest('js/build/'))
});


gulp.task('autoprefix', function () {
    gulp.src('css/*.css')
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('css/'))
});

gulp.task('tinypng', function () {
    gulp.src('img/*.png')
        .pipe(imagemin('API_KEY'))
        .pipe(gulp.dest('img/'));
});